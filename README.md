# Transactions manager

1.  Introduction
2.  Installation
    1.  Intializing the project
    2.  Web server configuration
    3.  Configuring MySql
3.  Final thoughts

##1. Introduction

The project was created using [Silex](https://silex.symfony.com//) as a base framework and it uses twig for templating. 

I mainly focused on data integrity and optimization while using good programing standards.

This project is created as a test evaluation, below you will find the requirements of the application:


---
Create a web app in PHP, which will gather and store data in a MySQL database.

Endpoint: /save?user=123&transaction=9999&amount=12.34. This must store the data sent via GET in a MySQL database, in a table with the needed structure for this.

A reporting page must also be created at /report. This should display the following:

Top 5 users, by transactions - displayed as a list, with user ID and transaction count

Top 5 users, by purchase amount - displayed as a list, with user ID and amount

Total amount evolution, per day (for all users summed up) - displayed in a table or in a graph

Each of the above report is subject to a selectable period, one of: last week / last month / all time

Reset button, for deleting all the data

---

##2. Installation
### Intializing the project

Clone the project from the following repository

    git clone https://bitbucket.org/michel-cetina/omniconvert_test
    
Run composer in order to fetch all the external libraries used

    composer update
    
### Web server configuration
The repository contains it's own .htaccess to handle the routes rewrites and the only requirement is to have mod_rewrite enabled. Below you will find an example of how I personally configured my apache vhost.

    <VirtualHost *:80>
        ServerAdmin webmaster@dummy-host.example.com
        DocumentRoot "C:/path/to/project/web"
        ServerName server.name.com
        ServerAlias server.alias
        ErrorLog "logs/server.name.com.error.log"
        CustomLog "logs/server.name.com.access.log" common
    	<Directory "C:/path/to/project/web">
            Options Indexes FollowSymLinks
    		AllowOverride All
    		Require all granted
        </Directory>
    </VirtualHost>
    
### Configuring MySql
A script is provided in order to create the DB structure. And can be found in the following path: *data/database-strucutre.sql*
    
    cd /path/to/your/root/folder
    mysql -uroot -p your_db_name
    MariaDB [(your_db_name)]> source data/database-strucutre.sql;
    
The project also contains a dummy file with a total amount of 1'261.792 records and it can be found here: *data/dump.7z* uncompress the file and load it into your DB

    MariaDB [(your_db_name)]> source data/dump.sql
    
As a final step you have to configure PDO in order to access the DB. Modify this file *src/App/Db/PdoInstance.php* adjusting the connector variables to your local environment

    self::$instance = new PDO(
                    "mysql:dbname=your_db_name;host=your_host_ip",
                    "db_user_name",
                    "db_user_password"
                );
                
After this point you should be able to access the report via http://server.name.com/report and also insert data via http://server.name.com/save?user=123&transaction=9999&amount=12.34

## 3. Final thoughts

The project was organized following an MVC pattern. Separating the controllers, data model and views completely.

The only controller and the only model service classes were both defined as a Service Provider in Silex. Givin both classes the ability to work as independent entities that could be easily migrated to other frameworks.

I added a logger on the main service (*TransactionsManager.php*) in order to capture errors during inserts and updates on the DB. This feature provides to the system administrator a tool to debug errors in a more friendly way while also having a way to track the exact error returned by the DB.

The database engine used was [InnoDB](https://dev.mysql.com/doc/refman/5.7/en/innodb-storage-engine.html) even though the queries performance was sometimes two times slower than  [MyiSam](https://dev.mysql.com/doc/refman/5.7/en/myisam-storage-engine.html) I preferred to choose a slower query time while gaining transactional operations when inserting and updating data. The way the system was described it's more oriented to be a heavy writing application and that was also one of the reason why I choose InnoDB.  

I improved the response time on the weekly and monthly reports by caching the response for a whole day (This data won't ever be modified and I can guarantee that the result won't change in the future).

