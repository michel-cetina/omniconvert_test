<?php

use Silex\Application;
use Silex\Provider\ServiceControllerServiceProvider;
use App\Controller\DataStorageController;
use App\Model\TransactionsManager;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new TwigServiceProvider());

// Initialize twig
$app['twig'] = $app->extend('twig', function ($twig, $app) {
    // add custom globals, filters, tags, ...

    return $twig;
});
// Create our transactions service
$app['service.transactions'] = function () use ($app) {
    return new TransactionsManager($app['monolog']);
};

// Create our controller sending the required services
$app['storage.controller'] = function () use ($app) {
    return new DataStorageController($app['request_stack']->getCurrentRequest(), $app['twig'], $app['service.transactions']);
};

// Map the routes with the controller actions
$app->get('/save', "storage.controller:saveDataAction");
$app->get('/report', 'storage.controller:getMainPageAction');
$app->get('/get-top-transactions', 'storage.controller:getTopUsersByTransactionAction');
$app->get('/get-top-purchases', 'storage.controller:getTopUsersByPurchasedAmountAction');
$app->get('/get-total-amounts', 'storage.controller:getTotalAmountAction');
$app->get('/reset-data', 'storage.controller:resetDataAction');

return $app;
