<?php

namespace App\Model;

/**
 * This class will transform data between the model and the view
 */
class DataDecorator
{
    public function __construct()
    {
    }

    /**
     * @param array $data   An array with the following structure ['total'=>'Total amount', 'month'=>'Month', 'day'=>'Day', 'year'=>'Year']
     * @return array        Returns the decorated array ['y'=>Total amount, 'label'=>The date of the transactions]
     */
    public static function decorateForGraph($data)
    {
        $result = array();
        foreach ($data as $item) {
            $result[] = array(
                'y'=>floatval($item['total']),
                'label'=>$item['month'].'-'.$item['day'].'-'.$item['year']
            );
        }
        return $result;
    }
}
