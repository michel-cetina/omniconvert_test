<?php

namespace App\Model;

use App\Db\QueryManager;
use MongoDB\BSON\Timestamp;
use Monolog\Logger;

/**
 * This class will act as a service in our system and it will understand all the business logic
 */
class TransactionsManager
{
    private $queryExecutor = null;
    private $logger = null;

    /**
     * Initialize the connection with the DB
     * @param Logger $logger    The monolog service that will help us logging errors
     */
    public function __construct(Logger $logger)
    {
        $this->queryExecutor = new QueryManager();
        $this->logger = $logger;
    }

    /**
     * This method will save the data received as parameters, if any errors happen during the execution the result of the method will be false and
     * the exact error produced will be logged.
     * @param $userId int           The user identifier
     * @param $transactionId int    The transaction identifier
     * @param $amount float         The amount purchased
     * @return array    An array with the following information: ['result'=>true or false, 'errorMessage'=>The error message]
     */
    public function storeData($userId, $transactionId, $amount)
    {
        $return = $this->queryExecutor->insertRecord($userId, $transactionId, $amount);
        $this->handleTaskResponse(__FUNCTION__, $return[0], $return[1]);
        return array('result'=>$return[0], 'errorMessage'=>$return[1]);
    }

    /**
     * This method will return a list of users ordered by transactions quantity
     * @param $from     Timestamp from where the data should be fetched
     * @param $to       Timestamp until the data should be fetched
     * @return array    An array with the following information: ['result'=>true or false, 'errorMessage'=>The error message, 'data'=>The data fetched from the DB. ['user_id', 'num_transactions']]
     */
    public function getUsersOrderedByTransaction($from = null, $to = null)
    {
        $return = $this->queryExecutor->selectUsersByTransactions($from, $to);
        return array('result'=>$return[0], 'errorMessage'=>$return[1], 'data'=>$return[2]);
    }

    /**
     * This method will return a list of users ordered by purchased amounts
     * @param $from     Timestamp from where the data should be fetched
     * @param $to       Timestamp until the data should be fetched
     * @return array    An array with the following information: ['result'=>true or false, 'errorMessage'=>The error message, 'data'=>The data fetched from the DB. ['user_id', 'amount_purchased']]
     */
    public function getUsersOrderedByPurchasedAmount($from = null, $to = null)
    {
        $return = $this->queryExecutor->selectUsersByPurchasedAmount($from, $to);
        return array('result'=>$return[0], 'errorMessage'=>$return[1], 'data'=>$return[2]);
    }

    /**
     * This method will return a list days with the amounts spend on each of them
     * @param $from     Timestamp from where the data should be fetched
     * @param $to       Timestamp until the data should be fetched
     * @return array    An array with the following information: ['result'=>true or false, 'errorMessage'=>The error message, 'data'=>The data fetched from the DB. ['day', 'month', 'year', 'amount_purchased']
     */
    public function getTotalAmounts($from = null, $to = null)
    {
        $return = $this->queryExecutor->selectTotalAmount($from, $to);
        return array('result'=>$return[0], 'errorMessage'=>$return[1], 'data'=>$return[2]);
    }

    /**
     * This method will clear all the data in our data tables. If there is an error during the execution the logger will save it
     */
    public function emptyData()
    {
        $return = $this->queryExecutor->emptyData();
        $this->handleTaskResponse(__FUNCTION__, $return[0], $return[1]);
        return array('result'=>$return[0], 'errorMessage'=>$return[1]);
    }

    /**
     * Process a task result and log it in case it fails (For the moment only modifiers are bing logged)
     */
    private function handleTaskResponse($methodName, $result, $errorMessage)
    {
        if (false == $result) {
            $this->logger->addCritical($methodName.' FAILED '.$errorMessage);
        }
    }
}
