<?php

namespace App\Controller;

use App\Model\DataDecorator;
use App\Model\TransactionsManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * This class will serve as a controller and it will work as a bridge between the model and the views
 */
class DataStorageController
{
    private $request;
    private $twig;
    private $manager;

    /**
     * Constructs the instance initializing all the services required
     * @param Request $request  The symfony request instance
     * @param \Twig_Environment  $twig  The twig engine template
     * @param TransactionsManager $manager  The model service that understand the logic of the application
     */
    public function __construct(Request $request, \Twig_Environment $twig, TransactionsManager $manager)
    {
        $this->request = $request;
        $this->twig = $twig;
        $this->manager = $manager;
    }

    /**
     * Saves the data sent by parameter and returns a json with the result of the transaction (Errors won't be exposed to the final user and only admins will be able to debug them)
     * @return JsonResponse
     */
    public function saveDataAction()
    {
        $result = $this->manager->storeData($this->request->get('user'), $this->request->get('transaction'), $this->request->get('amount'));
        return new JsonResponse(array('success'=>$result['result']));
    }

    /**
     * Renders the main page from where the user can view the data
     */
    public function getMainPageAction()
    {
        // An array of options that will calculate the date ranges required
        $options = array(
            'last-week'=>'?from='.strtotime('monday last week').'&to='.strtotime('sunday last week'),
            'last-month'=>'?from='.strtotime('first day of last month').'&to='.strtotime('last day of last month')

        );
        return $this->twig->render('index.html.twig', array('options'=>$options));
    }

    /**
     * Returns a list of users ordered by the quantity of transactions
     * @return Response
     */
    public function getTopUsersByTransactionAction()
    {
        $result = $this->manager->getUsersOrderedByTransaction($this->request->get('from'), $this->request->get('to'));
        return new Response($this->twig->render('top5-transactions.html.twig', array('data'=>$result['data'])), Response::HTTP_OK, $this->getResponseHeaders($this->request->get('from'), $this->request->get('to')));
    }

    /**
     * Returns a list of users ordered by the amount of their transactions
     * @return Response
     */
    public function getTopUsersByPurchasedAmountAction()
    {
        $result = $this->manager->getUsersOrderedByPurchasedAmount($this->request->get('from'), $this->request->get('to'));
        return new Response($this->twig->render('top5-purchases.html.twig', array('data'=>$result['data'])), Response::HTTP_OK, $this->getResponseHeaders($this->request->get('from'), $this->request->get('to')));
    }

    /**
     * Returns a graph containing all the transactions amounts segregated by days
     * @return Response
     */
    public function getTotalAmountAction()
    {
        $result = $this->manager->getTotalAmounts($this->request->get('from'), $this->request->get('to'));
        return new Response($this->twig->render('total-amounts.html.twig', array('data'=>json_encode(DataDecorator::decorateForGraph($result['data'])))), Response::HTTP_OK, $this->getResponseHeaders($this->request->get('from'), $this->request->get('to')));
    }

    /**
     * Flushes all the data from the tables and initializes the auto increments
     */
    public function resetDataAction()
    {
        $result = $this->manager->emptyData();
        if (!$result['result']) {
            return new RedirectResponse('report', 500);
        }
        return new RedirectResponse('/report');
    }

    /**
     * This method will help us setting a cache directive on the response headers for selected URLs that don't change during the time and can be cached
     * @param $from int The start date
     * * @param $from int The end date
     * @return array
     */
    private function getResponseHeaders($from = null, $to = null)
    {
        $headers = array();
        if (null !== $from && null !== $to) { // Only cache the requests that won't be modified
            $secondsToCache = 86400; // Caches the content for a whole day
            $ts = gmdate("D, d M Y H:i:s", time() + $secondsToCache) . " GMT";
            $headers['Expires'] = $ts;
            $headers['Pragma'] = 'cache';
            $headers['Cache-Control'] = 'max-age='.$secondsToCache;
        }
        return $headers;
    }
}
