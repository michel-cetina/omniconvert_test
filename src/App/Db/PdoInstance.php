<?php

namespace App\Db;

use PDO;

/**
 * A singleton class that will instance a new PDO connection
 */
class PdoInstance
{
    private static $instance = null;

    protected function __construct()
    {
    }

    private function __clone()
    {
    }

    /**
     * Returns the PDO instance
     * @return PDO
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new PDO(
                "mysql:dbname=test_omn;host=127.0.0.1",
                "root",
                ""
            );
        }

        return self::$instance;
    }
}
