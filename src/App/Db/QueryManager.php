<?php

namespace App\Db;

use MongoDB\BSON\Timestamp;

/**
 * This class will communicate with the DB providing the model a persistence layer
 */
class QueryManager
{
    private $pdo;

    /**
     * Initialize the class fetching a PDO connection from the singleton factory
     */
    public function __construct()
    {
        $this->pdo = PdoInstance::getInstance();
    }


    /**
     * Inserts a new record to the transactions table. This method will check first if the user has already a record in the aggregated table for the current date, creating it or updating it
     * and after that the record will be inserted in the transactions table.
     * The whole insert flow is being handled as a transaction.
     */
    public function insertRecord($userId, $transactionId, $amount)
    {
        $errorInfo = '';
        $result = false;
        try {
            $this->pdo->beginTransaction();
            $date = date('Y-m-d');
            $sql = "UPDATE transactions_agregated SET value = value + :amount, count = count + 1 WHERE user_id = :userId AND created_date = :createdDate";
            $stmt = $this->pdo->prepare($sql);
            $result = $stmt->execute(array(':userId'=>$userId, ':amount'=>$amount, ':createdDate'=>$date));

            if (!$result) {
                throw new \Exception(implode('-', $stmt->errorInfo()), $stmt->errorCode());
            }

            if ($stmt->rowCount() == 0) {
                $sql = "INSERT INTO transactions_agregated (user_id, value, count, created_date) VALUES(:userId, :amount, 1, :createdDate)";
                $stmt = $this->pdo->prepare($sql);
                $result = $stmt->execute(array(':userId'=>$userId, ':amount'=>$amount, ':createdDate'=>$date));

                if (!$result) {
                    throw new \Exception(implode('-', $stmt->errorInfo()), $stmt->errorCode());
                }
            }

            $sql = "INSERT INTO transactions (user_id, value, transaction_id, created_date) VALUES(:userId, :amount, :transacionId, :createdDate)";
            $stmt = $this->pdo->prepare($sql);
            $result = $stmt->execute(array(':userId'=>$userId, ':amount'=>$amount, ':transacionId'=>$transactionId, ':createdDate'=>date('Y-m-d H:i:s')));

            if (!$result) {
                throw new \Exception(implode('-', $stmt->errorInfo()), $stmt->errorCode());
            }
            $this->pdo->commit();
        } catch (\Exception $e) {
            $errorInfo = $e->getMessage();
            $this->pdo->rollBack();
        }

        return array($result, $errorInfo);
    }


    /**
     * Empties the data in our tables (transactions and transactions_agregated)
     */
    public function emptyData()
    {
        $errorInfo = '';
        $result = false;
        try {
            $this->pdo->beginTransaction();

            $sql = "TRUNCATE TABLE transactions";
            $stmt = $this->pdo->prepare($sql);
            $result = $stmt->execute();

            if (!$result) {
                throw new \Exception(implode('-', $stmt->errorInfo()), $stmt->errorCode());
            }

            $sql = "TRUNCATE TABLE transactions_agregated";
            $stmt = $this->pdo->prepare($sql);
            $result = $stmt->execute();

            if (!$result) {
                throw new \Exception(implode('-', $stmt->errorInfo()), $stmt->errorCode());
            }
            $this->pdo->commit();
        } catch (\Exception $e) {
            $errorInfo = $e->getMessage();
            $this->pdo->rollBack();
        }

        return array($result, $errorInfo);
    }

    /**
     * Fetches the top 5 users with the highest number of transactions. This method can retrieve all data or filter it between a range of dates
     * @param   $from   Timestamp from where the data should be fetched
     * @param   $to     Timestamp until the data should be fetched
     * @return  array   An array with the following structure:  [0] => The result of the transaction (true or false)
     *                                                          [1] => The MySql error information
     *                                                          [2] => The data fetched from the DB. ['user_id', 'num_transactions']
     */
    public function selectUsersByTransactions($from = null, $to = null)
    {
        if (null !== $from && null !== $to) {
            $from = date('Y-m-d', $from);
            $to = date('Y-m-d', $to).' 23:59:59';
        }

        $sql= "SELECT user_id, SUM(count) AS total FROM transactions_agregated ";

        if (null !== $from) {
            $sql.= "WHERE created_date BETWEEN :firstDay AND :lastDay ";
        }
        $sql.= "GROUP BY user_id ORDER BY COUNT(*) DESC LIMIT 5";

        $stmt = $this->pdo->prepare($sql);
        $result = $stmt->execute(array(':firstDay'=>$from, ':lastDay'=>$to));
        $errorInfo = $stmt->errorCode().' '.implode('-', $stmt->errorInfo());

        return array($result, $errorInfo, $stmt->fetchAll(\PDO::FETCH_ASSOC));
    }

    /**
     * Fetches the top 5 users with the highest purchased amounts. This method can retrieve all data or filter it between a range of dates
     * @param   $from   Timestamp from where the data should be fetched
     * @param   $to     Timestamp until the data should be fetched
     * @return  array   An array with the following structure:  [0] => The result of the transaction (true or false)
     *                                                          [1] => The MySql error information
     *                                                          [2] => The data fetched from the DB. ['user_id', 'amount_purchased']
     */
    public function selectUsersByPurchasedAmount($from = null, $to = null)
    {
        if (null !== $from && null !== $to) {
            $from = date('Y-m-d', $from);
            $to = date('Y-m-d', $to).' 23:59:59';
        }

        $sql = "SELECT user_id, SUM(value) AS total FROM transactions_agregated ";

        if (null !== $from) {
            $sql.= "WHERE created_date BETWEEN :firstDay AND :lastDay ";
        }

        $sql.= "GROUP BY user_id ORDER BY SUM(value) DESC LIMIT 5";

        $stmt = $this->pdo->prepare($sql);
        $result = $stmt->execute(array(':firstDay'=>$from, ':lastDay'=>$to));
        $errorInfo = implode('-', $stmt->errorInfo());

        return array($result, $errorInfo, $stmt->fetchAll(\PDO::FETCH_ASSOC));
    }

    /**
     * Fetches daily spent amounts for all users. This method can retrieve all data or filter it between a range of dates
     * @param   $from   Timestamp from where the data should be fetched
     * @param   $to     Timestamp until the data should be fetched
     * @return  array   An array with the following structure:  [0] => The result of the transaction (true or false)
     *                                                          [1] => The MySql error information
     *                                                          [2] => The data fetched from the DB. ['day', 'month', 'year', 'amount_purchased']
     */
    public function selectTotalAmount($from = null, $to = null)
    {
        if (null !== $from && null !== $to) {
            $from = date('Y-m-d', $from);
            $to = date('Y-m-d', $to).' 23:59:59';
        }

        $sql = "SELECT DAY(created_date) AS day, MONTH(created_date) AS month, YEAR(created_date) AS year, SUM(value) AS total FROM transactions_agregated ";

        if (null !== $from) {
            $sql.= "WHERE created_date BETWEEN :firstDay AND :lastDay ";
        }
        $sql.= "GROUP BY YEAR(created_date), MONTH(created_date), DAY(created_date) ORDER BY YEAR(created_date), MONTH(created_date), DAY(created_date) ASC";

        $stmt = $this->pdo->prepare($sql);
        $result = $stmt->execute(array(':firstDay'=>$from, ':lastDay'=>$to));
        $errorInfo = implode('-', $stmt->errorInfo());

        return array($result, $errorInfo, $stmt->fetchAll(\PDO::FETCH_ASSOC));
    }
}
