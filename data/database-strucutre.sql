# The main table containing all the transactions executed
CREATE TABLE `transactions` (
  `user_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `value` float NOT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`transaction_id`),
  KEY `transactions_user_id_value_created_date_index` (`user_id`,`value`,`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# The aggregated table that will speed up the queries
CREATE TABLE `transactions_agregated` (
  `user_id` int(11) NOT NULL,
  `value` float NOT NULL,
  `count` int(11) NOT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  KEY `transactions_user_id_value_created_date_index` (`user_id`,`value`,`count`,`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;